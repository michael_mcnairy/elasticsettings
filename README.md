ElasticSettings let's the developer pre-determine what fields of a table are going to be used in elasticsearch leaf query types.

For example, let's say you are building a program to search through your personal movie collection. You do a quick search for The Princess Bride on omdbapi.com (https://www.omdbapi.com/?s=the%20princess%20bride) to get some details to put into your database and it comes back like this.

{
    Title: "The Princess Bride",
    Year: "1987",
    imdbID: "tt0093779",
    Type: "movie",
    Poster: "https://images-na.ssl-images-amazon.com/images/M/MV5BMGM4M2Q5N2MtNThkZS00NTc1LTk1NTItNWEyZjJjNDRmNDk5XkEyXkFqcGdeQXVyMjA0MDQ0Mjc@._V1_SX300.jpg"
}

Your standard elasticsearch boolquery has four leaf types: must, must_not, filter, and should.

Okay, when I search for a movie, I'm most likely searching for the title. So, let's make that a "must".
Since they are making so many reboots these days I might want to "filter" by year. To reduce the result set and speed things up I may want to "filter" by type (movie, series, episode).

$leafs = [
    'Title' => 'must',
    'Year' => 'filter'
    'Type' => 'filter',
];

Pass in the associative array to an instance ElasticSettings and it will send a properly formatted query to your elasticsearch server when you provide a Title, and/or Year and Type.

It's a work in progress, but I'm having fun with it so far.
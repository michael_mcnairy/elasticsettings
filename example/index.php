<?php
    require __DIR__ . '/vendor/autoload.php';

    $response = "";
    $params   = $_GET;

    function search()
    {
         /*
         * Leaf query clauses look for a particular value in a particular field, such as the match, term or range queries.
         */
        $clauses = [
            'Title'  => 'must',
            'Year'   => 'filter',
            'imdbID' => 'must',
            'Type'   => 'filter'
        ];

        $search = new ElasticSettings\ElasticSettings('movies', 'videos', $clauses);
        return $search->results();
    }

    function render($results)
    {
        $html = "<ul>";

        foreach($results as $result)
        {
            $html .= "<li>{$result['Title']}</li>";
        }

        $html .="</ul>";

        return $html;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Movie Search</title>
    </head>
    <body>
        <form method="GET" action="/">
            <fieldset>
                <label for="Title">Title</label>
                <input type="text" name="Title" required />
                <br/><br/>

                <label for="Type">Type</label>
                <select name="Type"\>
                    <option value="movie"/>Movie</option>
                    <option value="series"/>Series</option>
                    <option value="episode">Episode</option>
                </select>
                <br/><br/>

                <label for="Year">Year</label>
                <input type="text" name="Year" />
                <br/><br/>

                <input type="submit" name="Submit">
                <br/><br/>
        </fieldset>
    </form>
        <?php
            if(empty($params)) {
                echo "No parameters have been provided.";
            } else {
                $results = search($params);

                if(empty($results))
                {
                    echo "Not results found.";
                } else {
                    echo render($results);
                }
            }
        ?>
    </body>
</html>
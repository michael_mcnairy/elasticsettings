<?php

namespace ElasticSettings;

use Elasticsearch\ClientBuilder;

class ElasticSettings {
    private $_index = '';
    private $_type = '';
    private $_clauses = [];

    public function __construct($index, $type, $clauses) {
        $this->_index = $index;
        $this->_type = $type;
        $this->_clauses = $clauses;

        $params = $_SERVER['QUERY_STRING'];
        $this->_clauses = $this->processParameters($params);
    }

        /**
     * Display search results
     * @param  string URL parameters
     * @return object  Blade view
     */
    public function results()
    {
        $query  = $this->boolQuery($this->_clauses);
        $client = ClientBuilder::create()->build();
        $params = [
            'index' => $this->_index,
            'type'  => $this->_type,
            'body'  => $query
        ];

        $results         = $client->search($params);
        $results = $this->sourceToArray($results['hits']['hits']);
        return $results;
    }

    public function query($params = "") {
        
        if(empty($params))
        {
            return "No results found.";
        } else {
            return "process parameters here";
        }
    }

    /**
     * Parse URL parameters and assign them to Elasticsearc leaf clauses
     * @param  string URL encoded parameters
     * @return array  Body of an Elasticsearch query
     */
    protected function processParameters($params) {
        //decode the string
        $params = urldecode($params);
        parse_str($params, $params);

        //initialize arrays to hold clauses that will be used in query body
        $must              = [];
        $must_not          = [];
        $filter            = [];
        $query['must']     = [];
        $query['filter']   = [];

        //iterate over clauses and populate arrays
        foreach($this->_clauses as $k => $v)
        {
            switch ($v) {
                case 'must':
                    array_push($query['must'] , $k);
                    break;
                case 'filter':
                    array_push($query['filter'] , $k);
                    break;
            }
        }

        //iterate over inputs and assign to corresponding array
        foreach($params as $k => $v)
        {
            if(in_array($k, $query['must']))
            {
                $v = $this->stripNots($v);

                array_push($must, ['match' => ["{$k}" => "{$v}"]]);
            }

            if(in_array($k, $query['filter']))
            {
                //check for empty filter values, will break elasticsearch query
                if(!empty($v))
                {
                    array_push($filter, ['term' => ["{$k}" => "{$v}"]]);
                }
            }
        }

        $query['must']     = $must;
        $query['must_not'] = $this->processNots($params['Title']);

        //remove empty filter element from query array, will break elasticsearch query
        if(!empty($filter))
        {
            $query['filter']   = $filter;
        } else {
            unset($query['filter']);
        }

        return $query;
    }

    /**
     * strip out terms preceded with minus sign
     * return Elasticsearch match clause
     * @param  string $query URL title parameter
     * @return array        Array of parse terms
     */
    protected function processNots($query) {
        $must_not = [];

        preg_match_all("@\ -[a-zA-z0-9]+@", $query, $matches);

        for($i = 0; $i < count($matches[0]);$i++)
        {
            $matches[0][$i] = str_replace(' -', '', $matches[0][$i]);
        }

        foreach($matches[0] as $match)
        {
            array_push($must_not, ['match' => ["Title" => "{$match}"]]);
        }

        return $must_not;
    }

    /**
     * strip out terms preceded with minus sign (not terms)
     * @param  string $query URL Title parameter
     * @return string        URL Title parameter with not terms removed
     */
    protected function stripNots($query)
    {
        preg_match_all("@\ -[a-zA-z0-9]+@", $query, $matches);
        $not = implode($matches[0]);
        $query   = str_replace($not, '', $query);

        return $query;
    }

    /**
     * Construct Elasticsearch bool query from given paramters
     * @param  array $params Elasticserach leaf clauses
     * @return array body of Elasticsearch bool query
     */
    protected function boolQuery($params) {
        $q = [
            'query' => [
                'bool' => [
                    //add search clauses here
                ]
            ]
        ];

        if(!empty($params['must'])) {
            $q['query']['bool']['must'] = $params['must'];
        }

        if(!empty($params['must_not'])) {
            $q['query']['bool']['must_not'] = $params['must_not'];
        }

        if(!empty($params['filter'])) {
            $q['query']['bool']['filter'] = $params['filter'];
        }

        return $q;
    }

    /**
     * die and dump
     * @param  mixed $debug Anything to output to screen
     * @return string        debug info
     */
    public static function dd($debug) {
        
        if(!empty($debug))
        {
            print_r($debug);
        }

        $debug = debug_backtrace();

        die("<br/>" . __FILE__ . ":" . $debug[0]['line']);
    }

    /**
     * Copy the _source element from the elasticsearch results into an array for easier parsing
     * @param  array $results elasticsearch results 
     * @return array         array of _source elements from the elasticsearch results
     */
    private function sourceToArray($results)
    {
        $data = [];
        //iterate over each result
        foreach($results as $result)
        {
            //put _source element into new array
            array_push($data, $result['_source']);
        }
        
        //return new array
        return $data;
    }
}